# Go Shortest

[![](https://goreportcard.com/badge/github.com/hyperjiang/go-shortest)](https://goreportcard.com/report/github.com/hyperjiang/go-shortest)
[![Build Status](https://travis-ci.org/hyperjiang/go-shortest.svg?branch=master)](https://travis-ci.org/hyperjiang/go-shortest)


Demos of calculating shortest driving path by using Google Maps API

This is a golang version of [shortest-path](https://github.com/hyperjiang/shortest-path)

Instead of using cronjob, we fork goroutine to process the input asynchronous so the users do not need to wait for the route calculation during calls.

## Dependencies

We use [dep](https://github.com/golang/dep) for dependency management, if you want to compile the program by yourself you need to install it first and run `dep ensure`

## Installation(using docker)

```
# initialize the containers
docker-compose up --build -d
```
Currently the containers will bind ports `10086` and `10010` to the localhost

## Testing

**Submit locations**

```
curl -X POST http://localhost:10086/route -d '[["22.372081", "114.107877"],["22.284419", "114.159510"],["22.326442", "114.167811"]]'
```

**Get the shortest path result**

```
curl http://localhost:10086/route/{token}
```

**Unit test**

```
docker-compose exec go-shortest go test ./...
```


