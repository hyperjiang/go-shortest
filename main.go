package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"sync/atomic"
	"syscall"
	"time"

	"gitlab.com/hyperjiang/go-shortest/api"
	"gitlab.com/hyperjiang/go-shortest/model"
	"github.com/golang/glog"
	"github.com/hyperjiang/flagfile"
)

var (
	addr          string
	serverTimeout time.Duration
)

func main() {

	defer glog.Flush()

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		sig := <-sigs
		fmt.Println()
		fmt.Println(sig)
		// Wait until all the goroutines have done
		for c := atomic.LoadInt32(&model.ProcessCounter); c > 0; {
			time.Sleep(time.Second)
			c = atomic.LoadInt32(&model.ProcessCounter)
			fmt.Printf("%d goroutines working\n", c)
		}
		fmt.Println("server exiting")
		os.Exit(0)
	}()

	server := http.Server{
		Addr:              addr,
		Handler:           api.Router(),
		ReadTimeout:       serverTimeout,
		ReadHeaderTimeout: serverTimeout,
		WriteTimeout:      serverTimeout,
	}

	server.SetKeepAlivesEnabled(false)
	server.ListenAndServe()
}

func init() {
	flag.StringVar(&addr, "addr", ":80", "Address to listen and serve")
	flag.DurationVar(&serverTimeout, "server_timeout", time.Second, "http server timeout")
	flagfile.Parse()

	glog.Info(flagfile.All())
	glog.Flush()
}
