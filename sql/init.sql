CREATE TABLE IF NOT EXISTS `routes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `token` char(36) NOT NULL,
  `input` text NOT NULL DEFAULT '',
  `path` text NOT NULL DEFAULT '',
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `total_distance` int(11) NOT NULL DEFAULT 0,
  `total_time` int(11) NOT NULL DEFAULT 0,
  `error` text NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `routes_token_unique` (`token`)
) ENGINE=InnoDB;