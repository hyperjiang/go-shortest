FROM golang:1.9.2-alpine3.7
RUN apk add --no-cache git
WORKDIR /go/src/gitlab.com/hyperjiang/go-shortest
COPY . /go/src/gitlab.com/hyperjiang/go-shortest
RUN go-wrapper download
RUN go-wrapper install
CMD ["go-wrapper", "run", "-flagfile=./conf/default.conf"]