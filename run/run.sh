#!/bin/bash

pidof=$(which "pidof")
if [ -z $pidof ]; then
    echo 'Please install pidof'
    exit
fi

BASE_DIR=$(cd $(dirname $0);pwd)
mkdir -p {bin,conf,logs}

LOG_DIR=${BASE_DIR}/logs
PID_FILE=${LOG_DIR}/PID

SERVER_NAME=go-shortest
SERVER_BIN=${BASE_DIR}/bin/${SERVER_NAME}
CONF_FILE=${BASE_DIR}/conf/default.conf
CMD_LINE="${SERVER_BIN} -flagfile=${CONF_FILE}"

getpid() {
    echo `$pidof ${SERVER_NAME}`
}

log() {
    echo `date +"%F %T"` - $1
}

not_running() {
    log "ERROR - ${SERVER_NAME} is not running."
    cp /dev/null ${PID_FILE}
}

start() {
    if [ ! -f "${SERVER_BIN}" ];then
        log "ERROR - Can not find ${SERVER_BIN}"
        exit 1
    fi

    PID=`getpid`
    if [ x"${PID}" == x"" ];then
        cd ${BASE_DIR}
        mkdir -p ${LOG_DIR}
        nohup ${CMD_LINE} >> ${LOG_DIR}/${SERVER_NAME}.stdout.log 2>&1 &
        sleep 1
        echo `getpid` > ${PID_FILE}
        log "start ${SERVER_BIN}"
    else
        echo ${PID} > ${PID_FILE}
        log "ERROR - PID:${PID} exist. ${SERVER_NAME} is already running."
    fi
}

stop() {
    PID=`getpid`
    if [ x"${PID}" == x"" ];then
        not_running
    else
        kill -15 $PID
        while true
        do
            if test $( ps aux | awk '{print $2}' | grep -w "$PID" | grep -v 'grep' | wc -l ) -eq 0;then
                log "SUCCESS - ${SERVER_NAME} has been stopped."
                cp /dev/null ${PID_FILE}
                break
            else
                log "wait to stop..."
                sleep 1
            fi
        done
    fi
}

kill9() {
    PID=`getpid`
    if [ x"${PID}" == x"" ];then
        not_running
        exit 1
    else
        kill -9 $PID
    fi
}

restart() {
    log "restart."
    stop
    start
}

check() {
    PID=`getpid`
    if [ x"${PID}" == x"" ];then
        not_running
    else
        log "${SERVER_NAME} is running, PID[${PID}]"
    fi
}

monitor() {
    PID=`getpid`
    if [ x"${PID}" == x"" ];then
        start
    else
        echo ${PID} > ${PID_FILE}
    fi
}

case "$1" in
    "start")
    start;
    ;;
    "stop")
    stop;
    ;;
    "restart")
    restart;
    ;;
    "kill9")
    kill9;
    ;;
    "monitor")
    monitor;
    ;;
    "check")
    check;
    ;;
    *)
    echo "Usage: $(basename "$0") start/stop/restart/kill9/monitor/check"
    exit 1
esac
