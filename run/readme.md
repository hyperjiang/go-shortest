This directory is for people who want to compile and run the server manually without using docker.

You can compile the program in the root directory by `make` command, the executable will be stored in `run/bin`, and then you can run `./run.sh start` to start the server, you can stop the server by `./run.sh stop`

*Note that you may need to edit the config file conf/default.conf*