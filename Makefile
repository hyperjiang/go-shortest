DIR=$(PWD)/run/bin

all: release

release:
	GOBIN=$(DIR) go install -gcflags "-N -l"

debug:
	GOBIN=$(DIR) go install -gcflags "-N -l" -tags "debug"