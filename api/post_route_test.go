package api

import (
	"net/http/httptest"
	"strings"
	"testing"

	"gitlab.com/hyperjiang/go-shortest/model"
	"github.com/gorilla/mux"
)

func TestPostRoute(t *testing.T) {
	req1 := httptest.NewRequest("POST", "/route", nil)
	res1 := PostRoute(req1).(rspToken)
	if res1.Error != "Invalid input format" {
		t.Errorf("%v", res1.Error)
	}

	json := strings.NewReader(`
		[["22.372081", "114.107877"],["22.284419", "114.159510"],["22.326442", "114.167811"]]
	`)
	req2 := httptest.NewRequest("POST", "/route", json)
	res2 := PostRoute(req2).(rspToken)
	if res2.Token == "" {
		t.Error("Fail to get token")
	}

	token := res2.Token
	req3 := httptest.NewRequest("GET", "/route/"+token, nil)
	req3 = mux.SetURLVars(req3, map[string]string{"token": token})
	res3 := GetRoute(req3).(rspRoute)
	if res3.Status != "in progress" {
		t.Fail()
	}

	var m model.Route
	if err := m.FindByToken(token); err != nil {
		t.Errorf("%v", err)
	}
	if err := m.Process(); err != nil {
		t.Errorf("%v", err)
	}
	req4 := httptest.NewRequest("GET", "/route/"+token, nil)
	req4 = mux.SetURLVars(req4, map[string]string{"token": token})
	res4 := GetRoute(req4).(rspRoute)
	if res4.Status != "success" {
		t.Fail()
	}
}

func TestValidateRoute(t *testing.T) {
	tests := []struct {
		name    string
		input   string
		want    string
		wantErr bool
	}{
		{
			"test1",
			"[[\"1\",\"1\"],[\"2\",\"2\"]]",
			"[[\"1\",\"1\"],[\"2\",\"2\"]]",
			false,
		},
		{
			"test2",
			"[[\"1\",\"1\"],[\"1\",\"1\"],[\"2\",\"2\"]]",
			"[[\"1\",\"1\"],[\"2\",\"2\"]]",
			false,
		},
		{
			"test3",
			"[[\"1\",\"1\"],[\"2\",\"2\"],[\"1\",\"1\"],[\"1\",\"2\"]]",
			"[[\"1\",\"1\"],[\"2\",\"2\"],[\"1\",\"2\"]]",
			false,
		},
		{
			"test4",
			"[[\"1\",\"1\"],[\"2\",\"2\"],[\"1\",\"1\"],[\"1\",\"2\"]]",
			"[[\"1\",\"1\"],[\"2\",\"2\"],[\"1\",\"2\"]]",
			false,
		},
		{
			"test5",
			"[[\"1\",\"1\",\"1\"],[\"2\",\"2\"]]",
			"[[\"1\",\"1\"],[\"2\",\"2\"]]",
			false,
		},
		{
			"test6",
			"[[\"1\",\"1\"]]",
			"[[\"1\",\"1\"]]",
			true,
		},
		{
			"test7",
			"invalid json",
			"invalid json",
			true,
		},
		{
			"test8",
			"[[\"1\"],[\"2\",\"2\"]]",
			"[[\"1\"],[\"2\",\"2\"]]",
			true,
		},
		{
			"test9",
			"[[\"1\",\"1\"],[\"2\",\"a\"]]",
			"[[\"1\",\"1\"],[\"2\",\"a\"]]",
			true,
		},
		{
			"test10",
			"[[\"1\",\"1\"],[\"2\",\"2\"]],[\"3\",\"3\"]],[\"4\",\"4\"]],[\"5\",\"5\"]],[\"6\",\"6\"]],[\"7\",\"7\"]],[\"8\",\"8\"]],[\"9\",\"9\"]],[\"10\",\"10\"]],[\"11\",\"11\"]]",
			"[[\"1\",\"1\"],[\"2\",\"2\"]],[\"3\",\"3\"]],[\"4\",\"4\"]],[\"5\",\"5\"]],[\"6\",\"6\"]],[\"7\",\"7\"]],[\"8\",\"8\"]],[\"9\",\"9\"]],[\"10\",\"10\"]],[\"11\",\"11\"]]",
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ValidateRoute(tt.input)
			if (err != nil) != tt.wantErr {
				t.Errorf("ValidateRoute() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if len(got) != len(tt.want) {
				t.Errorf("ValidateRoute() = %v, want %v", got, tt.want)
			}
		})
	}
}
