package api

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"

	"gitlab.com/hyperjiang/go-shortest/model"
	"github.com/golang/glog"
	jsoniter "github.com/json-iterator/go"
)

// PostRoute submits the locations
func PostRoute(r *http.Request) interface{} {

	defer r.Body.Close()
	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return err
	}

	glog.Infof("request: %s", b)

	input, err := ValidateRoute(fmt.Sprintf("%s", b))
	if err != nil {
		return rspToken{Error: fmt.Sprint(err)}
	}

	var m model.Route

	m.Input = input

	if err := m.Create(); err != nil {
		glog.Error(err)
		return rspToken{Error: fmt.Sprint(err)}
	}

	return rspToken{Token: m.Token}
}

// remove duplicated elements
func removeDuplicated(locations [][2]string) [][2]string {
	tmp := make(map[string]int)
	for _, l := range locations {
		tmp[l[0]+"-"+l[1]] = 1
	}

	var result [][2]string
	for k := range tmp {
		t := strings.Split(k, "-")
		result = append(result, [2]string{t[0], t[1]})
	}

	return result
}

// ValidateRoute runs validation and return the filtered input
func ValidateRoute(input string) (string, error) {
	var locations [][2]string
	err := jsoniter.UnmarshalFromString(input, &locations)
	if err != nil {
		return input, ErrInvalidInput
	}

	for _, l := range locations {
		if l[0] == "" || l[1] == "" {
			return input, ErrInvalidInput
		}
		if _, err := strconv.ParseFloat(l[0], 32); err != nil {
			return input, ErrInvalidInput
		}
		if _, err := strconv.ParseFloat(l[1], 32); err != nil {
			return input, ErrInvalidInput
		}
	}

	// remove duplicated coordinates
	locations = removeDuplicated(locations)

	if len(locations) < 2 || len(locations) > 10 {
		return input, ErrNumOutOfRange
	}

	return jsoniter.MarshalToString(locations)
}
