package api

import (
	"net/http/httptest"
	"testing"

	"github.com/gorilla/mux"
)

func TestGetRoute(t *testing.T) {
	req := httptest.NewRequest("GET", "/route/xxx", nil)

	req = mux.SetURLVars(req, map[string]string{"token": "xxx"})

	res := GetRoute(req).(rspRoute)

	if res.Status != "failure" {
		t.Fail()
	}
}
