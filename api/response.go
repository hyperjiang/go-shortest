package api

type rspToken struct {
	Token string `json:"token,omitempty"`
	Error string `json:"error,omitempty"`
}

type rspRoute struct {
	Status        string      `json:"status"`
	Path          [][2]string `json:"path,omitempty"`
	TotalDistance int         `json:"total_distance,omitempty"`
	TotalTime     int         `json:"total_time,omitempty"`
	Error         string      `json:"error,omitempty"`
}
