package api

import (
	"net/http"

	"gitlab.com/hyperjiang/go-shortest/model"
	"github.com/gorilla/mux"
	jsoniter "github.com/json-iterator/go"
)

// GetRoute gets the shortest route result
func GetRoute(r *http.Request) interface{} {
	token := mux.Vars(r)["token"]

	var m model.Route
	if err := m.FindByToken(token); err != nil {
		return getRouteFail("Invalid token")
	}

	if m.Status == model.RouteStatusNew {
		return rspRoute{Status: "in progress"}
	} else if m.Status == model.RouteStatusFailure {
		return getRouteFail(m.Error)
	}

	var path [][2]string
	jsoniter.UnmarshalFromString(m.Path, &path)

	return rspRoute{
		Status:        "success",
		Path:          path,
		TotalDistance: m.TotalDistance,
		TotalTime:     m.TotalTime,
	}
}

func getRouteFail(msg string) rspRoute {
	return rspRoute{
		Status: "failure",
		Error:  msg,
	}
}
