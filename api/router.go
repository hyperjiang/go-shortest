package api

import (
	"net/http"

	"github.com/gorilla/mux"
)

var router = mux.NewRouter().StrictSlash(true)

var apis = map[string]map[string]handler{
	http.MethodGet: {
		"/route/{token}": GetRoute,
	},
	http.MethodPost: {
		"/route": PostRoute,
	},
}

// Router returns the http routers
func Router() *mux.Router {
	return router
}

func init() {
	for method, group := range apis {
		for path, fn := range group {
			router.Methods(method).Path(path).HandlerFunc(wrap(fn))
		}
	}
}
