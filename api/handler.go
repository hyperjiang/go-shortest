package api

import (
	"net/http"

	"github.com/golang/glog"
	jsoniter "github.com/json-iterator/go"
)

// handler is the API request handler type
type handler func(r *http.Request) interface{}

// wrap returns json results
func wrap(fn handler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		rsp := fn(r)

		if body, err := jsoniter.Marshal(rsp); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			glog.Error(err)
		} else {
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusOK)
			w.Write(body)

			glog.Infof("%s %s returns: %s", r.Method, r.RequestURI, body)
		}
	}
}
