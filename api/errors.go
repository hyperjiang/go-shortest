package api

import (
	"errors"
)

// API errors
var (
	ErrInvalidInput  = errors.New("Invalid input format")
	ErrNumOutOfRange = errors.New("The number of locations should be between 2 and 10")
)
