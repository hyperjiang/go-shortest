package logic

import (
	"reflect"
	"testing"
)

func TestGetMatrix(t *testing.T) {
	type args struct {
		locations [][2]string
	}
	tests := []struct {
		name string
		args args
		want Matrix
	}{
		{
			"test1",
			args{
				[][2]string{
					{"22.372081", "114.107877"},
					{"22.284419", "114.159510"},
					{"22.326442", "114.167811"},
				},
			},
			Matrix{
				0: map[int][2]int{
					1: {15534, 1002},
					2: {9669, 849},
				},
				1: map[int][2]int{
					1: {0, 0},
					2: {8338, 898},
				},
				2: map[int][2]int{
					1: {8484, 967},
					2: {0, 0},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got, _ := GetMatrix(tt.args.locations); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetMatrix() = %v, want %v", got, tt.want)
			}
		})
	}
}
