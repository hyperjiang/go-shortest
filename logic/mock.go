package logic

import (
	"fmt"
	"net/http"
	"net/http/httptest"
)

type countingServer struct {
	s          *httptest.Server
	successful int
	failed     []string
}

// mockServerForQuery returns a mock server that only responds to a particular query string.
func mockServerForQuery(query string, code int, body string) *countingServer {
	server := &countingServer{}

	server.s = httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if query != "" && r.URL.RawQuery != query {
			server.failed = append(server.failed, r.URL.RawQuery)
			http.Error(w, "fail", 999)
			return
		}
		server.successful++

		w.WriteHeader(code)
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		fmt.Fprintln(w, body)
	}))

	return server
}

// Create a mock HTTP Server that will return a response with HTTP code and body.
func mockServer(code int, body string) *httptest.Server {
	serv := mockServerForQuery("", code, body)
	return serv.s
}

// default mock server
func defaultServer() *httptest.Server {
	response := `{
		"destination_addresses" : [
		   "Laguna City, Central, Hong Kong",
		   "789 Nathan Rd, Mong Kok, Hong Kong"
		],
		"origin_addresses" : [
		   "11 Hoi Shing Rd, Chai Wan Kok, Hong Kong",
		   "Laguna City, Central, Hong Kong",
		   "789 Nathan Rd, Mong Kok, Hong Kong"
		],
		"rows" : [
		   {
			  "elements" : [
				 {
					"distance" : {
					   "text" : "15.5 km",
					   "value" : 15534
					},
					"duration" : {
					   "text" : "17 mins",
					   "value" : 1002
					},
					"status" : "OK"
				 },
				 {
					"distance" : {
					   "text" : "9.7 km",
					   "value" : 9669
					},
					"duration" : {
					   "text" : "14 mins",
					   "value" : 849
					},
					"status" : "OK"
				 }
			  ]
		   },
		   {
			  "elements" : [
				 {
					"distance" : {
					   "text" : "1 m",
					   "value" : 0
					},
					"duration" : {
					   "text" : "1 min",
					   "value" : 0
					},
					"status" : "OK"
				 },
				 {
					"distance" : {
					   "text" : "8.3 km",
					   "value" : 8338
					},
					"duration" : {
					   "text" : "15 mins",
					   "value" : 898
					},
					"status" : "OK"
				 }
			  ]
		   },
		   {
			  "elements" : [
				 {
					"distance" : {
					   "text" : "8.5 km",
					   "value" : 8484
					},
					"duration" : {
					   "text" : "16 mins",
					   "value" : 967
					},
					"status" : "OK"
				 },
				 {
					"distance" : {
					   "text" : "1 m",
					   "value" : 0
					},
					"duration" : {
					   "text" : "1 min",
					   "value" : 0
					},
					"status" : "OK"
				 }
			  ]
		   }
		],
		"status" : "OK"
	 }`
	return mockServer(200, response)
}
