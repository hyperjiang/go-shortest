package logic

import (
	"flag"

	"github.com/golang/glog"
	"golang.org/x/net/context"
	"googlemaps.github.io/maps"
)

var (
	googleAPIKey string
	client       *maps.Client
)

// GetMatrix uses Google Maps Distance Matrix API to get the distance and duration matrix
func GetMatrix(locations [][2]string) (Matrix, error) {

	matrix := make(Matrix)

	origins := makeCoordinates(locations)
	destinations := origins[1:]

	r := &maps.DistanceMatrixRequest{
		Origins:       origins,
		Destinations:  destinations,
		DepartureTime: `now`,
		Units:         `UnitsMetric`,
		Mode:          maps.TravelModeDriving,
	}
	resp, err := client.DistanceMatrix(context.Background(), r)
	if err != nil {
		glog.Errorf("%v", err)
		return matrix, err
	}

	for i, row := range resp.Rows {
		matrix[i] = make(map[int][2]int)
		for j, ele := range row.Elements {
			if ele.Status != "OK" { // dead route
				matrix[i][j+1] = [2]int{-1, -1}
			} else {
				matrix[i][j+1] = [2]int{
					ele.Distance.Meters,
					int(ele.Duration.Seconds()),
				}
			}
		}
	}

	return matrix, nil
}

// change [lat, lon] to "lat,lon"
func makeCoordinates(locations [][2]string) []string {
	var res []string
	for _, location := range locations {
		res = append(res, location[0]+","+location[1])
	}
	return res
}

func init() {
	flag.StringVar(
		&googleAPIKey,
		"google_api_key",
		"AIzaSyBxuRd_YQ7f1zzOTMY8Kj577WnAAGZPxTU",
		"Google API key")

	var err error
	if flag.Lookup("test.v") == nil { // normal run
		client, err = maps.NewClient(maps.WithAPIKey(googleAPIKey))
	} else { // run under go test
		client, err = maps.NewClient(maps.WithAPIKey(googleAPIKey), maps.WithBaseURL(defaultServer().URL))
	}
	if err != nil {
		glog.Errorf("%s", err)
	}
}
