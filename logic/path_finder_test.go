package logic

import (
	"math"
	"reflect"
	"testing"
)

func TestGetShortestPathWithMatrix(t *testing.T) {
	type args struct {
		locations [][2]string
		matrix    Matrix
	}
	tests := []struct {
		name string
		args args
		want PathInfo
	}{
		{
			"test1",
			args{
				[][2]string{{"0", "0"}, {"1", "1"}, {"2", "2"}},
				Matrix{
					0: map[int][2]int{
						1: {1, 1}, 2: {2, 2},
					},
					1: map[int][2]int{
						2: {3, 3},
					},
					2: map[int][2]int{
						1: {1, 1},
					},
				},
			},
			PathInfo{
				[][2]string{{"0", "0"}, {"2", "2"}, {"1", "1"}},
				3,
				3,
			},
		},
		{
			"test2",
			args{
				[][2]string{{"0", "0"}, {"1", "1"}, {"2", "2"}, {"3", "3"}},
				Matrix{
					0: map[int][2]int{
						1: {1, 1}, 2: {2, 2}, 3: {1, 1},
					},
					1: map[int][2]int{
						2: {3, 3}, 3: {2, 2},
					},
					2: map[int][2]int{
						1: {1, 1}, 3: {4, 4},
					},
					3: map[int][2]int{
						1: {3, 3}, 2: {3, 3},
					},
				},
			},
			PathInfo{
				[][2]string{{"0", "0"}, {"2", "2"}, {"1", "1"}, {"3", "3"}},
				5,
				5,
			},
		},
		{
			"test3",
			args{
				[][2]string{{"0", "0"}, {"1", "1"}, {"2", "2"}, {"3", "3"}},
				Matrix{
					0: map[int][2]int{
						1: {1, 1}, 2: {2, 2}, 3: {1, 1},
					},
					1: map[int][2]int{
						2: {3, 3}, 3: {2, 2},
					},
					2: map[int][2]int{
						1: {-1, -1}, 3: {4, 4},
					},
					3: map[int][2]int{
						1: {3, 3}, 2: {3, 3},
					},
				},
			},
			PathInfo{
				[][2]string{{"0", "0"}, {"1", "1"}, {"3", "3"}, {"2", "2"}},
				6,
				6,
			},
		},
		{
			"test4",
			args{
				[][2]string{{"0", "0"}, {"1", "1"}},
				Matrix{},
			},
			PathInfo{
				[][2]string{{"0", "0"}, {"1", "1"}},
				math.MaxInt32,
				math.MaxInt32,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getShortestPathWithMatrix(tt.args.locations, tt.args.matrix); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("getShortestPath() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetShortestPath(t *testing.T) {
	type args struct {
		locations [][2]string
	}
	tests := []struct {
		name string
		args args
		want PathInfo
	}{
		{
			"test1",
			args{
				[][2]string{
					{"22.372081", "114.107877"},
					{"22.284419", "114.159510"},
					{"22.326442", "114.167811"},
				},
			},
			PathInfo{
				[][2]string{
					{"22.372081", "114.107877"},
					{"22.326442", "114.167811"},
					{"22.284419", "114.159510"},
				},
				18153,
				1816,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got, _ := GetShortestPath(tt.args.locations); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetShortestPath() = %v, want %v", got, tt.want)
			}
		})
	}
}
