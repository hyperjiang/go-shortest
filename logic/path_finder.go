package logic

import (
	"math"

	"github.com/klaidliadon/next"
)

// PathInfo stores the shortest path info
type PathInfo struct {
	Path          [][2]string
	TotalDistance int
	TotalTime     int
}

// Matrix stores distance and duration for each pair of origin and destination
type Matrix map[int]map[int][2]int

// GetShortestPath gets the shortest path
func GetShortestPath(locations [][2]string) (PathInfo, error) {
	if locations == nil {
		return PathInfo{}, ErrEmptyLocations
	}

	matrix, err := GetMatrix(locations)
	if err != nil {
		return PathInfo{}, err
	}

	return getShortestPathWithMatrix(locations, matrix), nil
}

func getShortestPathWithMatrix(locations [][2]string, matrix Matrix) PathInfo {
	path := PathInfo{TotalDistance: 0, TotalTime: 0}

	var shortestPath []interface{}
	totalDistance, totalTime := math.MaxInt32, math.MaxInt32

	for v := range getPermutations(len(locations)) {
		v = append([]interface{}{0}, v...)
		distance, duration := calculate(v, matrix)
		if shortestPath == nil || totalDistance > distance {
			shortestPath, totalDistance, totalTime = v, distance, duration
		}
	}

	for _, v := range shortestPath {
		path.Path = append(path.Path, locations[v.(int)])
	}

	path.TotalDistance = totalDistance
	path.TotalTime = totalTime

	return path
}

func getPermutations(num int) <-chan []interface{} {
	var a []interface{}

	for i := 1; i < num; i++ {
		a = append(a, i)
	}

	return next.Permutation(a, num-1, false)
}

// calculate the distance and duration of the given route
func calculate(route []interface{}, matrix Matrix) (distance, duration int) {
	distance, duration = 0, 0
	for i := 0; i < len(route)-1; i++ {
		if v, ok := matrix[route[i].(int)][route[i+1].(int)]; !ok {
			distance, duration = math.MaxInt32, math.MaxInt32
			return
		} else if v == [2]int{-1, -1} {
			distance, duration = math.MaxInt32, math.MaxInt32
			return
		} else {
			distance += v[0]
			duration += v[1]
		}
	}
	return
}
