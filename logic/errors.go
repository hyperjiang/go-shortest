package logic

import (
	"errors"
)

// Logic errors
var (
	ErrEmptyLocations = errors.New("Locations can not be empty")
)
