package model

import (
	"flag"
	"sync"

	"github.com/golang/glog"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql" // mysql driver
)

// DBInstance is a singleton DB instance
type DBInstance struct {
	initializer func() interface{}
	instance    interface{}
	once        sync.Once
}

var (
	mysqlDSN string
	mysqlDB  *DBInstance
)

// Instance gets the singleton instance
func (i *DBInstance) Instance() interface{} {
	i.once.Do(func() {
		i.instance = i.initializer()
	})
	return i.instance
}

func mysqlInit() interface{} {
	db, err := gorm.Open("mysql", mysqlDSN)
	if err != nil {
		glog.Fatalf("Cannot connect to mysql server: %v", err)
	}

	// sql log
	db.LogMode(true)

	stdDB := db.DB()
	stdDB.SetMaxIdleConns(2)
	stdDB.SetMaxOpenConns(16)

	return db
}

// MySQL returns mysql instance
func MySQL() *gorm.DB {
	return mysqlDB.Instance().(*gorm.DB)
}

func init() {
	flag.StringVar(
		&mysqlDSN,
		"mysql_dsn",
		"root:@tcp(127.0.0.1:3306)/go_shortest?charset=utf8&timeout=10s&parseTime=True",
		"mysql",
	)

	mysqlDB = &DBInstance{initializer: mysqlInit}
}
