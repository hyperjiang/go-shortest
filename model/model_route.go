package model

import (
	"fmt"
	"sync/atomic"
	"time"

	"gitlab.com/hyperjiang/go-shortest/logic"
	"github.com/golang/glog"
	jsoniter "github.com/json-iterator/go"
	uuid "github.com/satori/go.uuid"
)

// Route is the table structure
type Route struct {
	ID            uint      `json:"id,omitempty"`
	Token         string    `json:"token,omitempty"`
	Input         string    `json:"input,omitempty"`
	Path          string    `json:"path,omitempty"`
	Status        int8      `json:"status,omitempty"`
	TotalDistance int       `json:"total_distance,omitempty"`
	TotalTime     int       `json:"total_time,omitempty"`
	Error         string    `json:"error,omitempty"`
	CreatedAt     time.Time `json:"created_at,omitempty"`
	UpdatedAt     time.Time `json:"updated_at,omitempty"`
}

// Route status
const (
	RouteStatusNew     = 0
	RouteStatusSuccess = 1
	RouteStatusFailure = 2
)

// ProcessCounter is a global counter for processing goroutines
var ProcessCounter int32

// TableName for gorm
func (Route) TableName() string {
	return "routes"
}

// Create a new record
func (v *Route) Create() error {

	other := Route{
		Token: uuid.NewV4().String(),
		Input: v.Input,
	}

	db := MySQL().Create(&other)

	if db.Error != nil {
		return db.Error
	} else if db.RowsAffected == 0 {
		*v = other
		return ErrKeyConflict
	}

	*v = other

	fmt.Printf("goroutines: %d\n", ProcessCounter)

	// fork a goroutine to process the input asynchronously
	go v.Process()

	// increase the goroutine counter by 1
	atomic.AddInt32(&ProcessCounter, 1)

	return nil
}

// FindByToken finds a record by token
func (v *Route) FindByToken(token string) error {
	db := MySQL().Where("token = ?", token).First(v)

	if db.RecordNotFound() {
		return ErrDataNotFound
	} else if db.Error != nil {
		return db.Error
	}

	return nil
}

// Process the input locations and find the shortest path
func (v *Route) Process() error {

	// process finished, decrease the counter by 1
	defer atomic.AddInt32(&ProcessCounter, -1)

	if v.Status != RouteStatusNew { // already processed
		return nil
	}

	var locations [][2]string

	jsoniter.UnmarshalFromString(v.Input, &locations)

	pathInfo, err := logic.GetShortestPath(locations)
	if err != nil {
		v.Status = RouteStatusFailure
		v.Error = err.Error()
		MySQL().Save(v)
		return ErrFailed
	}

	s, err := jsoniter.MarshalToString(pathInfo.Path)
	if err != nil {
		v.Status = RouteStatusFailure
		v.Error = err.Error()
		MySQL().Save(v)
		return err
	}

	glog.Infof("path: %s", s)

	v.Path = s
	v.TotalDistance = pathInfo.TotalDistance
	v.TotalTime = pathInfo.TotalTime
	v.Status = RouteStatusSuccess
	return MySQL().Save(v).Error
}
