package model

import (
	"testing"
)

func TestRoute_Process(t *testing.T) {

	var m Route

	m.Input = "[[\"22.372081\", \"114.107877\"],[\"22.284419\", \"114.159510\"],[\"22.326442\", \"114.167811\"]]"

	if err := m.Create(); err != nil {
		t.Errorf("%v", err)
	}

	token := m.Token

	var v Route
	if err := v.FindByToken(token); err != nil {
		t.Errorf("%v", err)
	}

	if err := v.Process(); err != nil {
		t.Errorf("%v", err)
	}
}
